package ru.petproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Afisha application
 *
 */
@SpringBootApplication
public class App {


    public static void main( String[] args ) {

        SpringApplication.run(App.class, args);
    }
}
